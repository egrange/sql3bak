program sql3bak;

{$WEAKLINKRTTI ON}
{$RTTI EXPLICIT METHODS([]) PROPERTIES([]) FIELDS([])}
{$APPTYPE CONSOLE}

uses
  Windows, SysUtils, SQLite3, dwsXPlatform;

{$SetPEFlags IMAGE_FILE_LARGE_ADDRESS_AWARE or IMAGE_FILE_RELOCS_STRIPPED}

function GetTickCount64 : Int64; stdcall; external kernel32;

function FileSize(const name : String) : Int64;
var
   info : TWin32FileAttributeData;
begin
   if GetFileAttributesEx(PChar(Pointer(name)), GetFileExInfoStandard, @info) then
      Result:=info.nFileSizeLow or (Int64(info.nFileSizeHigh) shl 32)
   else Result:=0;
end;

var
   ok : Boolean;
   rc : Integer;
   src, dst : TSQLiteDB;
   backup : TSQLiteBackup;
   stmt : TSQLiteStatement;
   srcName, dstName : String;
   srcNameA, dstNameA : RawByteString;
   errMsg : PAnsiChar;
   srcJournalMode : RawByteString;
   t, srcSize, walSize : Int64;
   pageSize : Integer;
begin
   ok := False;
   try
      if not Bind_SQLite3_DLL then
         raise Exception.Create('failed to bin sqlite3.dll');
      if ParamCount <> 2 then begin
         Writeln('sql3bak v16.10.6.0 - using SQLite ', SQLite3_DLL_Version);
         Writeln;
         Writeln('Syntax: sql3bak <source> <destination>');
         Exit;
      end;

      srcName := ParamStr(1);
      srcNameA := UTF8Encode(srcName);
      dstName := ParamStr(2);
      dstNameA := UTF8Encode(dstName);

      try
         rc := SQLite3_Open_v2(PAnsiChar(srcNameA), src, SQLITE_OPEN_READWRITE, nil);
         if rc <> SQLITE_OK then
            raise Exception.CreateFmt('Cannot open source "%s": %s', [srcName, SQLite3_ErrMsg16(src)]);

         rc := SQLite3_Open_v2(PAnsiChar(dstNameA), dst, SQLITE_OPEN_READWRITE or SQLITE_OPEN_CREATE or SQLITE_OPEN_EXCLUSIVE, nil);
         if rc <> SQLITE_OK then
            raise Exception.CreateFmt('Cannot open destination "%s": %s', [dstName, SQLite3_ErrMsg16(src)]);

         SQLite3_Prepare16_v2(src, 'PRAGMA journal_mode', -1, stmt.Handle, nil);
            stmt.Step;
            srcJournalMode := stmt.ColumnText8(0);
         stmt.Finalize;

         srcSize := FileSize(srcName);
         Write('Source database is ', srcSize div 1024, ' kB');

         if SameText(String(srcJournalMode), 'wal') then begin
            walSize := FileSize(srcName+'-wal');
            WriteLn(' + ', walSize div 1024, ' kB WAL');
            if walSize > 0 then begin
               SQLite3_Prepare16_v2(src, 'PRAGMA page_size', -1, stmt.Handle, nil);
                  stmt.Step;
                  pageSize := stmt.ColumnInt(0);
               stmt.Finalize;
               if walSize > 1024*1024*1024 then
                  walSize := 1024*1024*1024;
               walSize := walSize div pageSize;
               if walSize > 2000 then begin
                  Writeln('Using cache_size = ', walSize, ' (page_size = ', pageSize, ')');
                  SQLite3_Exec(src, PAnsiChar(RawByteString('PRAGMA cache_size='+IntToStr(walSize))), nil, nil, errMsg);
               end;
            end;
         end else begin
            Writeln;
         end;

         SQLite3_Exec(src, 'PRAGMA busy_timeout=10000', nil, nil, errMsg);
         SQLite3_Exec(src, 'PRAGMA temp_store=MEMORY', nil, nil, errMsg);

         SQLite3_Exec(dst, 'PRAGMA synchronous=OFF', nil, nil, errMsg);
         SQLite3_Exec(dst, 'PRAGMA journal_mode=TRUNCATE', nil, nil, errMsg);
         SQLite3_Exec(dst, 'PRAGMA temp_store=MEMORY', nil, nil, errMsg);
         SQLite3_Exec(dst, 'PRAGMA cache_size=1', nil, nil, errMsg);

         backup := SQLite3_BackupInit(dst, 'main', src, 'main');
         if backup = nil then
            raise Exception.CreateFmt('Cannot initialize backup: %s', [SQLite3_ErrMsg16(src)]);

         rc := SQLite3_BackupStep(backup, 0);
         if not rc in [SQLITE_BUSY, SQLITE_LOCKED, SQLITE_OK] then
            raise Exception.CreateFmt('Backup start error: %s', [SQLite3_ErrMsg16(src)]);

         WriteLn('Backing up ', SQLite3_BackupRemaining(backup), ' pages from "', srcName, '"...');

         t := GetTickCount64;

         repeat
            rc := SQLite3_BackupStep(backup, -1);
            if rc = SQLITE_LOCKED then
               Sleep(10);
         until (rc <> SQLITE_BUSY) and (rc <> SQLITE_LOCKED);

         WriteLn('Backed up ', SQLite3_BackupPageCount(backup), ' pages to "', dstName,
                 '" in ', FormatFloat('0.000', (GetTickCount64-t)*0.001), ' seconds.');

         rc := SQLite3_BackupFinish(backup);
         if rc <> SQLITE_OK then
            raise Exception.CreateFmt('Backup failed: %s', [SQLite3_ErrMsg16(src)])
         else ok := True;

         SQLite3_Exec(dst, PAnsiChar('PRAGMA journal_mode='+srcJournalMode), nil, nil, errMsg);

      finally
         SQLite3_Close(src);
         SQLite3_Close(dst);
      end;
   except
      on E: Exception do begin
         ok := False;
         Writeln('sql3bak: ', E.Message);
      end;
   end;
   if ok then
      ExitCode := 0
   else ExitCode := 1;
end.
