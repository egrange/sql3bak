# SQLite3 backup utility

SQLite3 high performance backup utility

Performs a backup of SQLite3 databases using the SQLite3 backup API, with settings optimized for large WAL mode databases backed up to new files (not overwriting an existing backup).

See https://www.delphitools.info/?p=3898

Syntax sql3bak <source> <destination>

